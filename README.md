Agriculture
===========

This addon allows you to create an Agriculture Controller, which will then plant trees in a large block around it.  
When the trees mature, you can configure it to cut them automatically using construction robots.  The tree type (excluding coral and dead trees) is
configurable.

Copyright Stuff
---------------
This mod uses fragments of code from both Treefarm and Test Mode, and some altered placeholder graphics from Factorio itself.  These fragments have been marked.

All other code is licensed under the MIT Open Source License.

Requirements
------------

* Factorio 0.12.26+
* [This](http://gitlab.com/N3X15/factorio-treefarm) treefarm fork, which has some needed things for Agriculture to work.
  * Memory leak fix.
  * More efficient storage of trees.
  * API interfaces for addon mods.
  
Usage
-----

* Research Agriculture.
* Build an Agriculture Controller and fill it with seeds (can be filled with an inserter).
* Right-click on it with another Agriculture Controller to configure it after placing.

TODO
----

* Add an overlay to show planting range
* Add coral.
* Give trees a small chance to die.
* Fix broken strings.
