require "defines"

-- Constants
TREETYPES = {
	nil, -- Random
	"tree-01",
	"tree-02",
	"tree-02-red",
	"tree-03",
	"tree-04",
	"tree-05",
	"tree-06",
	"tree-06-brown",
	"tree-07",
	"tree-08",
	"tree-08-brown",
	"tree-08-red",
	"tree-09",
	"tree-09-brown",
	"tree-09-red",
}
-- Tree Status Matching
TREE_STATUSES={
	{"germling"},        -- 1
	{"very-small-tree"}, -- 2 etc
	{"small-tree"},
	{"medium-tree"},
	{}
}

for _, tree in ipairs(TREETYPES) do
	if tree ~= nil then
		TREE_STATUSES[5][#TREE_STATUSES[5]] = tree
	end
end
CONTROLLER_RADIUS = 10 -- 7
TREE_LIMIT        = 50 -- 40
VERSION           = 13032016 -- DDMMYYY
SEEDTYPE          = "germling" -- Was "seeds"

local AgZone ={}
AgZone.__index = AgZone
setmetatable(AgZone, 
{
	__call = function (cls, ...)
		return cls.new(...)
	end,
})
function AgZone.new(entity,zone)
	local self = setmetatable({}, AgZone)
	self.controller = entity
	if zone ~= nil and zone.version == VERSION then
		self.surface  = zone.surface
		self.bbox     = zone.bbox
		self.map      = zone.map
		self.deforest = zone.deforest
		self.replant  = zone.replant
		self.typepref = zone.typepref
		self.stop     = zone.stop
	else
		self.bbox = {
			{x = entity.position.x - CONTROLLER_RADIUS, y = entity.position.y - CONTROLLER_RADIUS},
			{x = entity.position.x + CONTROLLER_RADIUS, y = entity.position.y + CONTROLLER_RADIUS}
		}
		self.surface = entity.surface
		self.map = {}
		self.deforest = false
		self.replant  = false
		self.typepref = nil
		self.stop     = false
	end
	self.version=VERSION
	return self
end
function AgZone:checkTreePlacement(x,y)
	for yo = -1,1 do
		for xo = -1,1 do
			local idx = self:getidx(x+xo,y+yo)
			if self.map[idx] then
				-- Obstacle
				if val == 1 then
					return false
				end
			end
		end
	end
	return true
end
function AgZone:setTreePlanted(x,y)
	for yo = -1,1 do
		for xo = -1,1 do
			local idx = self:getidx(x+xo,y+yo)
			if idx~=nil and math.abs(xo)+math.abs(yo) < 2 then
				self.map[idx] = true
			end
		end
	end
end
function AgZone:setTreeHarvested(x,y)
	for yo = -1,1 do
		for xo = -1,1 do
			local idx = self:getidx(x+xo,y+yo)
			if idx~=nil and math.abs(xo)+math.abs(yo) < 2 then
				self.map[idx] = false
			end
		end
	end
end
function AgZone:rebuildMap()
	self.map={}
	for y = 0,CONTROLLER_RADIUS*2 do
		for x = 0,CONTROLLER_RADIUS*2 do
			table.insert(self.map,false)
		end
	end
	for y = -CONTROLLER_RADIUS,CONTROLLER_RADIUS do
		for x = -CONTROLLER_RADIUS,CONTROLLER_RADIUS do
			local names = detectTreeAt(self.surface,{
				x=(x+self.controller.position.x),
				y=(y+self.controller.position.y),
			}, 0)
			if #names > 0 then
				self:setTreePlanted(x,y)
			end
		end
	end
end
function AgZone:getOpenPositions()
	local newPositions = {}
	for y = -CONTROLLER_RADIUS,CONTROLLER_RADIUS do
		for x = -CONTROLLER_RADIUS,CONTROLLER_RADIUS do
			if self:checkTreePlacement(x,y) then
				table.insert(newPositions,{x=x+self.controller.position.x,y=y+self.controller.position.y})
			end
		end
	end
	return newPositions
end
function AgZone:getidx(x,y)
	if x < 0 or x > CONTROLLER_RADIUS*2
	or y < 0 or y > CONTROLLER_RADIUS*2 then
		return nil
	end
	local i = y + (x*((CONTROLLER_RADIUS*2)))
	if i < 1 or i > (CONTROLLER_RADIUS*2)^2 then
		return nil
	end
	return i + 1
end

script.on_init(function()
	initTables()
end)


script.on_load(function()
	initTables()
end)

table.indexOf = function( t, object )
	local result
	 
	if "table" == type( t ) then
		for i=1,#t do
			if object == t[i] then
				result = i
				break
			end
		end
	end
	 
	return result
end

function tableContains(t, element)
	for _, value in pairs(t) do
		if value == element then
			return true
		end
	end
	return false
end

function iTableContains(t, element)
	for _, value in ipairs(t) do
		if value == element then
			return true
		end
	end
	return false
end

function bbox2Rect(bbox)
	return {
		{bbox[1].x-1,bbox[1].y-1},
		{bbox[2].x+1,bbox[2].y+1}
	}
end

-- From Testing Mode
function isHolding(player,name, min_count)
  local holding = player.cursor_stack
  --[[
  if holding == nil then
	game.player.print("holding = nil")
  end
  if holding.valid == false then
	game.player.print("holding invalid")
  end
  if not stack.valid then
	game.player.print("stack invalid")
  end
  if holding.name ~= stack.name then
	game.player.print("Name doesn't match")
  end
  if holding.count < stack.count then
	game.player.print("Count is less than desired.")
  end
  ]]--
  if (holding~=nil) and (holding.valid) and (holding.name == name) and (holding.count >= min_count) then
    return true
  end
  --[[
  game.player.print("holding.name == "..holding.name)
  game.player.print("holding.count == "..tostring(holding.count))
  game.player.print("stack.name == "..stack.name)
  game.player.print("stack.count == "..tostring(holding.count))
  ]]--
  return false
end

script.on_event(defines.events.on_put_item, function(event)
	local player = game.players[event.player_index]
	if isHolding(player,"ag-controller",1) then
		local selectedEnt = player.selected
		if not selectedEnt then
			--game.player.print("selectedEnt = nil")
			return
		end
		if not selectedEnt.valid then
			--game.player.print("Not valid.")
			return
		end
		if selectedEnt.name ~= "ag-controller" then
			--game.player.print("selectedEnt != ag-controller.")
			--game.player.print("selectedEnt.name = "..selectedEnt.name)
			return
		end
		if selectedEnt and selectedEnt.valid and selectedEnt.name == "ag-controller" then
			if not openZoneSettingsFor(player,selectedEnt) then
				createZoneFor(selectedEnt)
				openZoneSettingsFor(player,selectedEnt)
			end
		end
	else
		--game.player.print("Not holding ag-controller.")
	end
end)

function openZoneSettingsFor(player,entity)
	for k, zone in pairs(global.agriculture.zones) do
		if zone.controller.valid and zone.controller == entity then
			openZoneSettings(player,zone)
			return true
		end
	end
	return false
end

script.on_event(defines.events.on_gui_click, function(event)
	local player = game.players[event.player_index]
	local context = global.agriculture.context
	if event.element.name == "ag-deforest" then
		context.deforest = not context.deforest
	elseif event.element.name == "ag-replant" then
		context.replant = not context.replant
	elseif event.element.name == "ag-treetype" then
		local tti = table.indexOf(TREETYPES,context.typepref)
		if tti == nil then tti = 1 end
		tti = tti + 1
		if tti > #TREETYPES then 
			tti = 1 
		end
		context.typepref = TREETYPES[tti]
		local typecap = context.typepref
		if typecap == nil then 
			typecap="Random" 
		end
		--game.local_player.print("Zone will now attempt to grow trees of type "..typecap.." ("..tti..")")
		player.gui.center.agframe.settings["ag-treetype"].caption=typecap
	elseif event.element.name == "ag-clear" then
		local germlings = context.surface.find_entities_filtered{area = context.bbox, name="germling"}
		for _, germling in pairs(germlings) do
			if germling.valid and not germling.to_be_deconstructed(player.force) then
				germling.order_deconstruction(player.force)
			end
		end
		if context.typepref ~= nil then
			local trees = context.surface.find_entities_filtered{area = context.bbox, type="tree"}
			for _, tree in pairs(trees) do
				if tree.valid and not tree.to_be_deconstructed(player.force) then
					tree.order_deconstruction(player.force)
				end
			end
		end
	elseif event.element.name == "ag-save" then
		closeZoneSettings(player)
	end
end)

script.on_event(defines.events.on_entity_died, function(event)
	if event.entity.name == "ag-controller" then
		checkZoneValidity()
	end
end)

function closeZoneSettings(player)
	gui = player.gui.center
	if global.agriculture.context ~= nil then
		if global.agriculture.context.rebuildMap ~= nil then
			global.agriculture.context:rebuildMap()
			player.print("Found "..#global.agriculture.context:getOpenPositions().." open positions.")
			global.agriculture.context.stop=false
		end
	end
	global.agriculture.context=nil
	if gui.agframe then 
		gui.agframe.destroy()
	end
end

function openZoneSettings(player,zone)
	gui = player.gui.center
	if gui.agframe then 
		closeZoneSettings()
	end
	zone.stop=true
	global.agriculture.context=zone
	gui.add({ type="frame",direction="horizontal", name="agframe", caption="Agriculture Zone Settings" })
	
	gui.agframe.add({ type="table", name="settings", colspan=2 })
	
	gui.agframe.settings.add({ type="label", caption="Clear Grown Trees:" })
	gui.agframe.settings.add({ type="checkbox", name="ag-deforest", state = zone.deforest })
	
	gui.agframe.settings.add({ type="label", caption="Plant Seeds:" })
	gui.agframe.settings.add({ type="checkbox", name="ag-replant", state = zone.replant })
	
	gui.agframe.settings.add({ type="label", caption="Tree type:"})
	local treecap = zone.typepref
	if treecap == nil then
		treecap = "Random"
	end
	gui.agframe.settings.add({ type="button", name="ag-treetype", caption=treecap })
	
	gui.agframe.settings.add({ type="button", name="ag-clear", caption="CLEAR" })
	gui.agframe.settings.add({ type="button", name="ag-save", caption="Close" })
end

function checkZoneValidity()
	for k, zone in ipairs(global.agriculture.zones) do
		if not zone.controller.valid then
			table.remove(global.agriculture.zones, k)
			break
		end
	end
end

script.on_event(defines.events.on_player_mined_item, function(event)
	if event.item_stack.name == "ag-controller" then
		checkZoneValidity()
	end	
end)

function createZoneFor(entity)
	local newzone = AgZone(entity)
	local efficiency = {
		calcEfficiency(entity.surface, {x = newzone.bbox[1].x, y = newzone.bbox[1].y}),
		calcEfficiency(entity.surface, {x = newzone.bbox[2].x, y = newzone.bbox[1].y}),
		calcEfficiency(entity.surface, {x = newzone.bbox[1].x, y = newzone.bbox[2].y}),
		calcEfficiency(entity.surface, {x = newzone.bbox[2].x, y = newzone.bbox[2].y})
	}
	
	newzone.efficiency = (efficiency[1] + efficiency[2] + efficiency[3] + efficiency[4]) / 4
	newzone:rebuildMap()
	return newzone
end

function createAndOpenZoneFor(player,entity)
	local newzone = createZoneFor(entity)
	
	if newzone.efficiency == 0 then
		entity.destroy()
		player.character.insert{name="ag-controller", count=1}
		player.print("The soil is too poor.")
		return false
	end
	global.agriculture.zones[#global.agriculture.zones + 1] = newzone
	openZoneSettings(player,newzone)
	return true
end

script.on_event(defines.events.on_built_entity, function(event)
	local player = game.players[event.player_index]
	local surface = event.created_entity.surface
	if event.created_entity.name == "ag-controller" then
		createAndOpenZoneFor(player,event.created_entity)
	elseif event.created_entity.name == "ag-controller-overlay" then
		local e = surface.create_entity{name = "ag-controller", position = event.created_entity.position, force=player.force}
		createAndOpenZoneFor(player,e)
		event.created_entity.destroy()
	end
end)

function detectTreeAt(surface, pos, radius)
	if radius == nil then
		radius = 0
	end
	-- Check tile first.
	if surface.get_tile(pos.x,pos.y).name == "stone-path" then
		return {"stone-path"}
	end
	local detected = surface.find_entities{
		{pos.x-radius,pos.y-radius},
		{pos.x+radius,pos.y+radius}
	}
	local ignore_names={
		"smoke",
		"item-on-ground",
		"iron-ore",
		"copper-ore"
	}
	local names = {}
	for _,ent in pairs(detected) do
		if ent.valid and not tableContains(ignore_names,ent.name) then
			if not tableContains(names,ent.name) then
				names[#names+1]=ent.name
			end
		end
	end
	return names
end

-- Modified treefarm stuff.
script.on_event(defines.events.on_tick, function(event)
	global.agriculture.tick = global.agriculture.tick + 1
	if (global.agriculture.tick % 60) == 0 then
		for k, zone in pairs(global.agriculture.zones) do
			if zone.controller.valid and not zone.stop then
				local growchance = math.ceil(math.random()* 100)
				remote.call("treefarm","checktrees",{surface=zone.surface, area = zone.bbox, typepref = zone.typepref})
				local growntrees = zone.surface.find_entities_filtered{area = zone.bbox, type="tree"}
				if (growchance > 95) then
					--if math.random() <= zone.efficiency then
						local treeplaced = false					
						local openPositions = zone:getOpenPositions()
						-- Loop maximum of 49 times.
						while #openPositions > 0 do
							local growntree = {}
							local selectedIndex = math.random(#openPositions)
							local treeposition = table.remove(openPositions,selectedIndex)
							growntree = zone.surface.find_entities_filtered{area = {treeposition, treeposition}, type="tree"}
							if #growntree > 1 then
								for id=2,#growntree do
									growntree[id].destroy()
								end
							end
							if growntree[1] ~= nil then 
								break 
							end
							
							-- Now we're just checking to see if there's enough room since can_place_entity doesn't give a fuck about trees.
							obstacles = detectTreeAt(zone.surface,treeposition)
							if #obstacles == 0 then
								if #growntrees < TREE_LIMIT and zone.replant and zone.controller.get_item_count(SEEDTYPE) > 0 then
									--if script.can_place_entity{name="lab", position = treeposition} then -- 3x3
									if zone.surface.can_place_entity{name="ag-collider", position = treeposition} then -- 2x2ish
										zone.controller.get_inventory(1).remove{name = SEEDTYPE, count = 1}
										addTreeToFarm(zone.surface, treeposition, 1, zone.typepref)
										zone:setTreePlanted(treeposition.x-zone.controller.position.x,treeposition.y-zone.controller.position.y)
										--game.local_player.print("Added germling at pos "..serpent.dump(treeposition))
										treeplaced = true
										break
									end
								else
									break
								end
							else
								zone:setTreePlanted(treeposition.x-zone.controller.position.x,treeposition.y-zone.controller.position.y)
							end
						end -- (treeplaced~= true)
					--end
				end -- (growchance > 99) and (#growntrees < 40)
			end
		end -- _, field in ipairs(global.treefarm.field)
	end -- (global.treefarm.tick % 60) == 0
--[[
	if (global.agriculture.tick % 1200) == 0 then		
		for _, zone in ipairs(global.agriculture.zones) do
			if zone.controller.valid then
				if zone.controller.get_item_count("fertilizer") > 0 then
					zone.controller.get_inventory(1).remove{name = "fertilizer", count = 1}
				end
			end
		end -- _, field in ipairs(global.treefarm.field)
	end -- ((global.treefarm.tick + 30) % 60) == 0
]]--
	if (global.agriculture.tick % (300 + math.ceil(math.random()*300))) == 0 then		
		for k, zone in pairs(global.agriculture.zones) do
			if zone.controller.valid and zone.deforest and not zone.stop then	
				local bb = bbox2Rect(zone.bbox)
				local growntrees = zone.surface.find_entities_filtered{area = bb, type="tree"}
				if #growntrees > 5 then
					-- This is a predictable pattern, but I'd rather not deal with a table being resized while it's being iterated.
					local rnd_out = math.ceil(math.random()*5)
					local trees_cut = 0
					for _, tree in pairs(growntrees) do
						if tree.valid --[[and trees_cut + 1 <= rnd_out]] and detectTreeStatus(tree)==5 and not tree.to_be_deconstructed(zone.controller.force) then
							tree.order_deconstruction(zone.controller.force)
							trees_cut = trees_cut + 1
							zone:setTreeHarvested(tree.position.x-zone.controller.position.x,tree.position.y-zone.controller.position.y)
						end
					end
					checkZoneValidity()
					--game.local_player.print("Found "..#growntrees.." grown trees, "..trees_cut.." of which are being harvested.")
				else
					--game.local_player.print("Found "..#growntrees.." grown trees.")
				end
			end
		end
	end
end)

-- Modified treefarm stuff.
function calcEfficiency(surface,position)

	local tileEfficiency = 
	{
		["grass"] = 1.00,
		["grass-medium"] = 1.00,
		["grass-dry"] = 0.90,
		["dirt"] = 0.75,
		["dirt-dark"] = 0.75,
		["hills"] = 0.50,
		["sand"] = 0.30,
		["sand-dark"] = 0.30,

		["other"] = 0.01
	}
	local efficiency = 0
	local x,y,samples
	samples=0
	for x = -7, 7, 7 do
		for y = -7, 7 , 7 do
			tileName = surface.get_tile(position.x + x, position.y + y).name
			if tileEfficiency[tileName] == nil then
				tileName = "other"
			end
			efficiency = efficiency + tileEfficiency[tileName]
			samples = samples + 1
		end
	end

	efficiency = efficiency / samples
	return efficiency
end


function calcTreeEfficiency(position)
	local tileName = script.get_tile(position.x, position.y).name
	local tileInfo = global.agriculture.tile_info[tileName]
	if tileInfo == nil then
		return 0.00
	else
		return tileInfo.efficiency
	end
end

function zoneNeedsUpdate(zone, tmpl)
	return tmpl ~= zone["version"] or
		zone.rebuildMap == nil
end

-- Modified treefarm stuff.
function initTables()
	if global.agriculture == nil then
		global.agriculture = {}
	end

	if global.agriculture.zones == nil then
		global.agriculture.zones = {}
	else
		local upgradedZones=0
		for zk,zone in pairs(global.agriculture.zones) do
			if zoneNeedsUpdate(zone) then
				global.agriculture.zones[zk]=AgZone(zone.controller,zone)
			end
		end
		--if upgradedZones > 0 then
			--game.local_player.print("Updated "..upgradedZones.." zones.")
		--end
	end

	if global.agriculture.tick == nil then
		global.agriculture.tick = 0
	end

	if global.agriculture.efficiency == nil then
		global.agriculture.efficiency = {}
	end
	if global.agriculture.tile_info == nil then
		-- if script.get_tile(position.x, position.y).name == "grass" then
		global.agriculture.tile_info = {
			["grass"] = {
				efficiency = 1.00
			},
			["grass-medium"] = {
				efficiency = 1.00
			},
			["grass-dry"] = {
				efficiency = 1.00
			},
			["dirt"] = {
				efficiency = 0.95
			},
			["dirt-dark"] = {
				efficiency = 0.95
			},
			["hills"] = {
				efficiency = 0.70
			},
			["sand"] = {
				efficiency = 0.60
			},
			["sand-dark"] = {
				efficiency = 0.60
			},
			["default"] = {
				efficiency = 0.00
			}
		}
	end
	--game.local_player.print("Agriculture tables installed.")
end


remote.add_interface("agriculture",
{
  dump = function()
		script.show_message_dialog(serpent.line(global.agriculture))
  end
})

function addTreeToFarm(surface, entitypos, status, typepref)
	--game.local_player.print("Received addtree: "..typepref)
	remote.call("treefarm","addtree", surface, entitypos, status, typepref)
end

function detectTreeStatus(entity)
	for status, matchtable in ipairs(TREE_STATUSES) do
		for _, name in ipairs(matchtable) do
			--game.player.print(status.." "..name.." ? "..entity.name)
			if entity.name == name then
				return status
			end
		end
	end
	-- game.player.print("Unknown tree "..entity.name)
	return 5
end
